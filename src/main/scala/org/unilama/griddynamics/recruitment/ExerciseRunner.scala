package org.unilama.griddynamics.recruitment

import org.unilama.griddynamics.recruitment.model._

import scala.util.Random

object ExerciseRunner {
  def main(args: Array[String]): Unit = {
    val userStorage = new UserStorage(SimpleIndex[String, Long]())

    val users = sys.props.getOrElse("users", "100000").toInt
    val queries = sys.props.getOrElse("queries", "100").toInt
    val verbose = sys.props.getOrElse("verbose", "false").toBoolean

    val matrimonial = List("single", "married", "divorced")
    val gender = List("male", "female")
    val car = List("have_a_sports_car", "have_a_compact_car", "have_a_truck", "have_a_pickup")
    val state = List("state_NY", "state_WI", "state_MI", "state_TX", "state_IL")


    def randomAttributes(): Set[String] = {
      Set(
        matrimonial(Random.nextInt(matrimonial.size)),
        gender(Random.nextInt(gender.size)),
        car(Random.nextInt(car.size)),
        state(Random.nextInt(state.size))
      )
    }

    1 to users map ((_, randomAttributes())) foreach {
      case (a, b) =>
        userStorage.add(User(a, b))
    }

    1 to queries map ((_, randomAttributes())) map {
      case (key, value) => ( key, value, userStorage.countBy(AllOp, value) )
    } foreach {
      case (i, attributes, count) => if (verbose) println(s"Query #$i  $attributes count is $count")
    }
  }
}
