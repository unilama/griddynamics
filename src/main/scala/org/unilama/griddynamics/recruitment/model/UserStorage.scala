package org.unilama.griddynamics.recruitment.model

import scala.collection.mutable

case class User(id:Long, attributes: Set[String])

class UserStorage(index:Index[String, Long]) extends Counter{
  val users = mutable.Map[Long, User]()

  def add(user: User) = {
    // TODO optimize it to update index for changed attributes only
    if(users contains user.id )
      remove(user.id)
    users(user.id) = user
    user.attributes foreach(index.add(_, user.id))
  }

  def remove(userId:Long) ={
    users(userId).attributes foreach(index.remove(_, userId))
    users - userId
  }

  def getBy(operation: Operation, attributes: Set[String]) = {
    index get(attributes, operation) map { userId => users(userId) }
  }

  override def countBy(operation: Operation, attributes: Set[String]): Long = {
    index count(attributes, operation)
  }

  def byId(id: Long) = users get id
}

object UserStorage {
  def apply(index: Index[String,Long]): UserStorage = new UserStorage(index)
}

