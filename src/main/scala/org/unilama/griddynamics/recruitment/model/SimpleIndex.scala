package org.unilama.griddynamics.recruitment.model

import scala.collection.mutable

class SimpleIndex[Name,Id] extends Index[Name,Id] {
  var stats = mutable.HashMap[Name,Set[Id]]()

  private def subIndexes(names: Set[Name]):Set[Set[Id]] = names.flatMap( stats.get(_) )

  override def count(names: Set[Name], operation: Operation): Long = {
    get(names, operation).size
  }

  override def add(name: Name, id: Id): Unit = {
    val ids = stats get name match {
      case Some(statsSet) => statsSet + id
      case None => Set(id)
    }

    stats += (name -> ids)
  }

  override def get(names: Set[Name], operation: Operation): Set[Id] = {
    operation.combine( subIndexes(names) )
  }

  override def remove(name: Name, id: Id): Unit = {
    stats.get(name).map( _ - id ) map {
      ids => stats += name -> ids
    }
  }
}

object SimpleIndex extends SimpleIndex{
  def apply[Name,Id](): SimpleIndex[Name,Id] = new SimpleIndex[Name,Id]()
}
