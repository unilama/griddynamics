package org.unilama.griddynamics.recruitment.model


trait Index[Name,Id] {
  def remove(name: Name, id: Id)
  def count(names: Set[Name], operation: Operation):Long
  def add(name:Name, id:Id)
  def get(names: Set[Name], operation: Operation): Set[Id]
}


trait Operation {
  def combine[A](indexParts: Set[Set[A]]): Set[A]
}

object AllOp extends  Operation {
  override def combine[A](indexParts: Set[Set[A]]): Set[A] = {
    indexParts.foldLeft(Set.empty[A])((acc,value) => acc ++ value ).filter( id =>
      !indexParts.exists( !_.contains(id) )
    )
  }
}

object AnyOp extends Operation {
  override def combine[A](indexParts: Set[Set[A]]): Set[A] = {
    indexParts.foldLeft(Set.empty[A])((acc,value) => acc ++ value )
  }
}
