package org.unilama.griddynamics.recruitment.model

import org.scalatest.FlatSpec
import org.scalatest.Matchers._

class UserStorageTest extends FlatSpec {
  "UserStorage" should "allow add user without attributes" in {
    val userStorage = UserStorage(SimpleIndex[String,Long]())
    userStorage add User(1, Set())
    val result = userStorage byId 1
    result shouldBe defined
  }

  it should "return 0 for unknown attribute count" in {
    val userStorage = UserStorage(SimpleIndex[String,Long]())
    userStorage add User(1, Set("testX"))
    val result = userStorage countBy(AnyOp,Set("testY"))
    result shouldBe 0
  }

  it should "count attributes" in {
    val userStorage = UserStorage(SimpleIndex[String,Long]())
    userStorage add User(1, Set("testX"))
    userStorage add User(2, Set("testX"))
    val result = userStorage countBy(AnyOp,Set("testX"))
    result shouldBe 2
  }

  it should "get users by provided attributes" in {
    val userStorage = UserStorage(SimpleIndex[String,Long]())
    userStorage add User(1, Set("testX"))
    userStorage add User(2, Set("testX"))
    val result = userStorage getBy(AnyOp,Set("testX"))

    result should contain (User(1,Set("testX")))
    result should contain (User(2,Set("testX")))
    result should have size 2
  }

  it should "return empty result for unknown attributes" in {
    val userStorage = UserStorage(SimpleIndex[String,Long]())
    userStorage add User(1, Set("testZ"))
    val result = userStorage getBy(AnyOp,Set("testA"))
    result shouldBe empty
  }

  it should "update user attributes in index" in {
    val userStorage = UserStorage(SimpleIndex[String,Long]())
    userStorage add User(1, Set("testZ"))
    userStorage add User(1, Set("testX"))
    val result = userStorage getBy(AnyOp,Set("testX"))
    result should ( contain (User(1, Set("testX"))) and have size 1 )
  }

  it should "allow remove user" in  {
    val userStorage = UserStorage(SimpleIndex[String,Long]())
    userStorage add User(1, Set("testQ"))
    userStorage add User(2, Set("testQ"))
    userStorage remove 1
    val result = userStorage getBy(AnyOp,Set("testQ"))
    result should ( contain (User(2, Set("testQ"))) and have size 1 )
  }
}
