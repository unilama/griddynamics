package org.unilama.griddynamics.recruitment.model
import org.scalatest.FlatSpec
import org.scalatest.Matchers._

class IndexTest extends FlatSpec {
  "AllAop" should "return empty set for empty subIndexes" in {
      val subIndexes = Set(Set.empty[Long], Set.empty[Long])
      val result = AllOp.combine(subIndexes)
      result shouldBe empty
  }

  it should "return empty set for no subIndexes" in {
    val result = AllOp.combine(Set.empty[Set[Long]])
    result shouldBe empty
  }

  it should "get the intersection of all sets" in {
    val testSets:Set[Set[Long]] = Set( Set(1,2,4), Set(3,4), Set(2,4))
    val result = AllOp.combine(testSets)
    result should contain only 4L
  }

  "AnyOp" should "return empty set for empty subIndexes" in {
    val subIndexes = Set(Set.empty[Long], Set.empty[Long])
    val result = AnyOp.combine(subIndexes)
    result shouldBe empty
  }

  it should "return empty set for no subIndexes" in {
    val result = AnyOp.combine(Set.empty[Set[Long]])
    result shouldBe empty
  }

  it should "get union of all subIndexes" in {
    val testSets:Set[Set[Long]] = Set( Set(1,2,4), Set(3,4), Set(2,4,5), Set(6))
    val result = AnyOp.combine(testSets)
    result should contain allOf (1,2,3,4,6)
  }
}