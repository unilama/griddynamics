package org.unilama.griddynamics.recruitment.model


import org.scalatest.FlatSpec
import org.scalatest.Matchers._

class SimpleIndexTest extends FlatSpec {
  "Simple Index" should "allow adding elements" in {
        val simpleIndex = SimpleIndex[String,Int]()
        simpleIndex.add("test", 1)
        simpleIndex.add("test", 2)
        simpleIndex.add("test", 3)

        val result = simpleIndex.get(Set("test"), AnyOp)

        result should contain allOf(1,2,3)
  }

  it should "accept multiple names with same id" in {
    val simpleIndex = SimpleIndex[String,Int]()

    simpleIndex.add("test1",1)
    simpleIndex.add("test2",1)

    val result1 = simpleIndex.get(Set("test1"), AnyOp)
    result1 should contain only 1

    val result2 = simpleIndex.get(Set("test2"), AnyOp)
    result2 should contain only 1

  }

  it should "accept multiple id with same names" in {
    val simpleIndex = SimpleIndex[String,Int]()

    simpleIndex.add("test1",1)
    simpleIndex.add("test1",2)

    val result = simpleIndex.get(Set("test1"), AnyOp)
    result should contain allOf (1,2)
  }

  it should "allow accept duplicated id for name" in {
    val simpleIndex = SimpleIndex[String,Int]()

    simpleIndex.add("test10",1)
    simpleIndex.add("test10",1)

    val result = simpleIndex.get(Set("test10"), AnyOp)
    result should contain only 1
  }

  it should "count elements for same name" in {
    val simpleIndex = SimpleIndex[String,Int]()

    simpleIndex.add("test20",1)
    simpleIndex.add("test20",2)

    val result = simpleIndex.count(Set("test20"), AnyOp)
    result shouldBe 2
  }

  it should "give 0 count for empty index" in {
    val simpleIndex = SimpleIndex[String,Int]()
    val result = simpleIndex.count(Set("testAny"), AnyOp)
    result shouldBe 0
  }

  it should "give 0 count for not existing element" in {
    val simpleIndex = SimpleIndex[String,Int]()

    simpleIndex.add("testX",1)

    val result = simpleIndex.count(Set("testY"), AnyOp)
    result shouldBe 0
  }

  it should "remove id for name" in {
    val simpleIndex = SimpleIndex[String,Int]()

    simpleIndex.add("testX",1)
    simpleIndex.add("testX",12)
    simpleIndex.remove("testX",1)

    val result = simpleIndex.get(Set("testX"), AnyOp)
    result should (contain (12) and have size 1)
  }
}
