# Questions
## run some performance benchmarks and explain them:

to run benchmark use this command:
```
sbt run -Dusers=100000 -Dqueries=1000 -Dverbose=true
```
system properties can be ommited, fallback provided

Result for 100000 users on my local machine:

Queries  | Time[s]
------------- | -------------
100           | 4
1000          | 31
10000         | 353


## explain how this solution can scale(vertically or horizontally - your preference),

This solution can be scaled out by creating few instances of UserStorage and use them to store partition of Users.
That requires some logic that will route user to specific UserStorage and aggregeate partial queries to get final result.

## what do we have to do to make it run 2x faster,

Scale out, or use more efficient indexing

## is there any difference if we ask for 2 or 200 attributes?

In current implementation index relay on usinon of index parts, so more attributes means more unions to perform and will
slow down whole solution.
